import { Component, OnInit } from '@angular/core';
declare var google: any;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  mapkey = 'AIzaSyDVU0b1M6aNsetB-1cOBtMdv-9I5-1IN7I';
  url = 'https://maps.googleapis.com/maps/api/js?key=' + this.mapkey + '&callback=initMap&libraries=places';

 

  constructor() { }

  ngOnInit(): void {
    this.loadScript();
  }


  loadScript() {
    window['initMap'] = () => {
      this.initMap();
    };
    if (!window.document.getElementById('script')) {
      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      window.document.body.appendChild(node);
    } else {
      this.initMap();
    }
  }


  initMap = () => {
    let marker1: google.maps.Marker
    let marker2: google.maps.Marker;
    let poly: google.maps.Polyline
    let geodesicPoly: gogle.maps.Polyline;
    const map = new google.maps.Map(
      document.getElementById("map") as HTMLElement,
      {
        zoom: 4,
        center: { lat: 34, lng: -40.605 },
      }
    );
  
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(
      document.getElementById("info") as HTMLElement
    );
  
    marker1 = new google.maps.Marker({
      map,
      draggable: true,
      position: { lat: 40.714, lng: -74.006 },
    });
  
    marker2 = new google.maps.Marker({
      map,
      draggable: true,
      position: { lat: 48.857, lng: 2.352 },
    });
  
    const bounds = new google.maps.LatLngBounds(
      marker1.getPosition() as google.maps.LatLng,
      marker2.getPosition() as google.maps.LatLng
    );
    map.fitBounds(bounds);
  
    // google.maps.event.addListener(marker1, "position_changed", this.update());
    // google.maps.event.addListener(marker2, "position_changed", this.update());
  
    poly = new google.maps.Polyline({
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 3,
      map: map,
    });
  
    geodesicPoly = new google.maps.Polyline({
      strokeColor: "#CC0099",
      strokeOpacity: 1.0,
      strokeWeight: 3,
      geodesic: true,
      map: map,
    });
  
    const path = [
      marker1.getPosition() as google.maps.LatLng,
      marker2.getPosition() as google.maps.LatLng,
    ];
    poly.setPath(path);
    geodesicPoly.setPath(path);
    const heading = google.maps.geometry.spherical.computeHeading(
      path[0],
      path[1]
    );
    (document.getElementById("heading") as HTMLInputElement).value = String(
      heading
    );
    (document.getElementById("origin") as HTMLInputElement).value = String(
      path[0]
    );
    (document.getElementById("destination") as HTMLInputElement).value = String(
      path[1]
    );
  
  }



    
  }


